const Joi = require('joi');
const SchemaModel = require('../config/schema');

const optionalParams = [
	'orderId',
	'items',
	'tax',
	'discount',
	'coupon',
    'paymentStatus',
    'shipping',
    'billing',
    'items',
    'size',
    'subFrequency',
    'subUnit',
    'requestType',
    'totalPrice'
];

const attToGet = ['cartId','orderId', 'totalPrice','paymentStatus'];
const attToQuery = ['cartId','orderId','totalPrice','items','shipping'];

const cartSchema =  {
    hashKey: 'cartId',
    timestamps: true,
    schema: Joi.object({
		cartId: Joi.string().alphanum(),
		orderId:Joi.string(),
		items:Joi.object(), //array or object containing all products and plans in the cart
        totalPrice: Joi.number().positive(),
        tax: Joi.number().positive(),
        discount: Joi.number().positive(),
        coupon: Joi.string(),
        paymentStatus: Joi.string(), //if successful or failed
        shipping: Joi.object(), //delivery or shipping address
        billing: Joi.object(), //billing details like type of billing, phone etc.
        size: Joi.string(),
        subFrequency: Joi.number().min(30),
        subUnit: Joi.string(),
        requestType: Joi.string()
    }).unknown(true).optionalKeys(optionalParams)
};

const optionsObj = {
    attToGet,
    attToQuery,
};
const Cart = SchemaModel(cartSchema,optionsObj); // create model by passing the table name of the store

module.exports = Cart;